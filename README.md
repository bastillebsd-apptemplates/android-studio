## Android Studio

Bastille App Template to bootstrap Android Studio

## Enable Linux Compat

```shell
sysrc linux_enable=YES  
```
## Add Devfs Rules to allow adb usb debugging

```shell
echo -e "[devfsrules_usb_jail=10] \nadd include \$devfsrules_hide_all \nadd include \$devfsrules_unhide_basic \nadd include \$devfsrules_unhide_login \nadd path fuse unhide \nadd path usb* mode 0666 unhide \nadd path 'usb/*' mode 0666 unhide \nadd path 'usbctl' mode 660 unhide \nadd path ugen* mode 0666 unhide" >> /etc/defaults/devfs.rules 
```
## Create

```shell
bastille create TARGET 13.1-RELEASE 127.0.1.2 
```
**YOU MUST ASSIGN A LOOPBACK IP FROM THE RANGE: 127.0.1.1/8 AS 127.0.1.2, OTHERWISE GRADLE WILL FAIL**

## Bootstrap

```shell
bastille bootstrap https://gitlab.com/bastillebsd-apptemplates/android-studio.git 
```
## Usage

```shell
bastille template TARGET https://gitlab.com/bastillebsd-apptemplates/android-studio.git --arg XDG_RUNTIME_DIR=$XDG_RUNTIME_DIR 
```

## Allow Xorg Jail to Host (exec like you user)

```shell
 xhost +
```
## Launch Android Studio (USB)

```shell
 bastille cmd TARGET studio
```
## Launch Android Studio (WIFI/LAN)

```shell
 bastille cmd TARGET studio YOUR_DEVICE_IP
```
## Gradle FreeBSD to Linux (Add on build.gradle "Proyect")

```shell
buildscript {
    System.setProperty("os.name","Linux")
}
```


## Screenshot

![Screenshot](https://gitlab.com/bastillebsd-apptemplates/android-studio/-/raw/main/Screenshots/20220928_13h52m04s.png)

